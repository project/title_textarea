
-- SUMMARY --

Title Textarea module allows entity titles/labels to be textareas (with HTML).

For a full description of the module, visit the project page:
  http://drupal.org/project/title_textarea
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/title_textarea

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* @todo


-- USAGE --

* @todo


-- CONTACT --

Current maintainers:
* Jelle Sebreghts (Jelle_S) - http://drupal.org/u/jelle_s
