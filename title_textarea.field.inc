<?php

/**
 * @file
 * Implement a title field formater.
 */


/**
 * Implements hook_field_formatter_info().
 */
function title_textarea_field_formatter_info() {
  return array(
    'title_textarea_linked' => array(
      'label' => t('Linked'),
      'field types' => array('text_long'),
      'settings' => array('title_textarea_link' => ''),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function title_textarea_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $settings = $instance['display'][$view_mode]['settings'];
  $element = array();

  $link_types = array(
    'content' => t('Content'),
  );
  $element['title_textarea_link'] = array(
    '#title' => t('Link title to'),
    '#type' => 'select',
    '#default_value' => $settings['title_textarea_link'],
    '#empty_option' => t('Nothing'),
    '#options' => $link_types,
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function title_textarea_field_formatter_settings_summary($field, $instance, $view_mode) {
  $settings = $instance['display'][$view_mode]['settings'];
  $summary = array();

  $link_types = array(
    'content' => t('Linked to content'),
  );
  // Display this setting only if field is linked.
  if (isset($link_types[$settings['title_textarea_link']])) {
    $summary[] = $link_types[$settings['title_textarea_link']];
  }
  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function title_textarea_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'];
  $output = isset($items[0]) ? $items[0]['safe_value'] : '';

  if (!empty($output) && $settings['title_textarea_link'] == 'content') {
    $uri = entity_uri($entity_type, $entity);
    $output = l($output, $uri['path'], array('html' => TRUE));
  }

  $element = array(
    array(
      '#markup' => $output,
    ),
  );

  return $element;
}
